#pragma once
// should be template
class PriorityQueue {
	int  _maxSize;
	int  _size;
	int* _heap;
public:
	PriorityQueue(int maxSize): _maxSize(maxSize), _size(0), _heap(new int[maxSize]) { }
	
	int parent(int i) { return i / 2; }
	int leftChild(int i) { return i * 2; }
	int rightChild(int i) { return i * 2 + 1; }

	void siftUp(int i);
	void siftDown(int i);
	void insert(int p);
	int  extractMax();
	void remove(int i);
	void chagePriority(int i, int p);
	void show(); 
	// heap sort 
};