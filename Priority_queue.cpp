#include <iostream>
#include "Priority_queue.h"

void swap(int &a, int &b) 
{
	int tmp = a;
	a = b, b = tmp;
}

void PriorityQueue::siftUp(int i) 
{
	while (i > 1 && _heap[this->parent(i)] < _heap[i]) 
	{
		swap(_heap[this->parent(i)], _heap[i]);
		i = this->parent(i);
	}
}

void PriorityQueue::siftDown(int i)
{
	int maxIndex = i, l, r;
		l = this->leftChild(i);
		if (l <= _size && _heap[l] > _heap[maxIndex])
			maxIndex = l;

		r = this->rightChild(i);
		if (r <= _size && _heap[r] > _heap[maxIndex])
			maxIndex = r;

		if (i != maxIndex) {
			swap(_heap[i], _heap[maxIndex]);
			this->siftDown(i +1);
		}
}

void PriorityQueue::insert(int p)
{
	if (_size != _maxSize)
	{
		_size++;
		_heap[_size] = p;
		this->siftUp(_size);
	}
}

int PriorityQueue::extractMax()
{
	int result = _heap[1];
	_heap[1] = _heap[_size];
	this->siftDown(1);
	_size--;
	return result;
}

void PriorityQueue::remove(int i)
{
	_heap[i] = INT_MAX;
	this->siftUp(i);
	this->extractMax();

}

void PriorityQueue::chagePriority(int i, int p)
{
	int old  = _heap[i];
	_heap[i] = p;
	if (p > old)
		this->siftUp(i);
	else 
		this->siftDown(i);
}

void PriorityQueue::show()
{
	for (int i = 1, cnt = 1; i < _size; cnt *= 2)
	{
		for (int j = i * 2; j < _size; j++)
		{
			std::cout << " ";
		}
		for ( int j = 0; j < cnt && i <= _size; j++, i++)
		{
			std::cout << _heap[i] << " ";
		}
	    std::cout << " \n \n";
	}
	std::cout << "\n";
}