#include <iostream>
#include "Priority_queue.h"
using namespace std;

void main() 
{
	PriorityQueue example(15);
	example.insert(42);
	example.insert(29);
	example.insert(18);
	example.insert(14);
	example.insert(7);
	example.insert(18);
	example.insert(12);
	example.insert(11);
	example.insert(13);
	example.show();
	example.insert(51);
	//example.insert(35);
	example.show();
	example.extractMax();
	example.extractMax();
	example.show();
	
	system("pause");
}